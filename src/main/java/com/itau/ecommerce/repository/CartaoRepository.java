package com.itau.ecommerce.repository;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.ecommerce.model.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String> {
	
	Optional<Cartao> findByNumeroCartao(String numeroCartao);

}
