package com.itau.ecommerce.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itau.ecommerce.model.Cartao;
import com.itau.ecommerce.model.CartaoContrato;
import com.itau.ecommerce.model.CartaoContratoResposta;
import com.itau.ecommerce.repository.CartaoRepository;

@Controller
public class CartaoController {
	
	@Autowired
	CartaoRepository cartaoRepository;
	
	@CrossOrigin
	@RequestMapping(path="/cadCartao", method=RequestMethod.POST)
//	public ResponseEntity<?> cadastrarCartao(HttpServletRequest request, @RequestBody Cartao cartao) {
	public ResponseEntity<?> cadastrarCartao(@RequestBody Cartao cartao) {	
//		String bearer = request.getHeader("Authorization");
		
//		String token = bearer.replace("Bearer ", "");
		
//		String idUsuario = tokenService.verificar(token);
//		Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
		
//		if (usuarioBanco.isPresent()){
//			Usuario usuario = new Usuario();
//			usuario.setFuncional(idUsuario);
//			atividade.setUsuario(usuario);
			cartao = cartaoRepository.save(cartao);
			return ResponseEntity.ok(cartao);
//		}
		
//		return ResponseEntity.badRequest().body("Atividade não cadastrada, usuário não encontrado.");	
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/autorizacao", method=RequestMethod.POST)
	public ResponseEntity<?>  validaCartao(@RequestBody CartaoContrato cartao) {
		
    	CartaoContratoResposta resposta = new CartaoContratoResposta();
		Cartao aux = new Cartao();
		
		aux.setNumeroCartao(cartao.getNumeroCartao());
		aux.setCodigoSeguranca(cartao.getCodigoSeguranca());
		aux.setDataValidade(cartao.getDataValidade());
		
		Optional<Cartao> cartaoBanco = cartaoRepository.findByNumeroCartao(aux.getNumeroCartao());
		
		if (cartaoBanco.isPresent()) {
			
		   if (cartaoBanco.get().isAtivo() == false) {
			   resposta.setCodigoResposta(06);
			   resposta.setMensagemResposta("Cartao inativo.");
			   return ResponseEntity.ok(resposta);
		   }
		
		   if (cartaoBanco.get().validaCodigoSeguranca(cartao.getCodigoSeguranca()) == false) {
		      resposta.setCodigoResposta(01);
			  resposta.setMensagemResposta("Codigo de seguranca invalido.");
			  return ResponseEntity.ok(resposta);
		   }
		
		   if (cartaoBanco.get().validaDataValidade(cartao.getDataValidade()) == false) {
			  resposta.setCodigoResposta(02);
			  resposta.setMensagemResposta("Data de validade invalida.");
			  return ResponseEntity.ok(resposta);
		   }
		   if (cartaoBanco.get().validaNomeClienteCartao(cartao.getNomeClienteCartao()) == false) {
			   resposta.setCodigoResposta(03);
			   resposta.setMensagemResposta("Nome do cliente invalido.");
			   return ResponseEntity.ok(resposta);
		   }
		   
		   if (cartaoBanco.get().getSaldoLimite() < cartao.getValor()) {
			   resposta.setCodigoResposta(04);
			   resposta.setMensagemResposta("Saldo insulficiente.");
			   return ResponseEntity.ok(resposta);
		   }	  
		   
		   // Atualiza limite do cartao
		   cartaoBanco.get().setSaldoLimite(cartaoBanco.get().getSaldoLimite() - cartao.getValor());
		   
		   aux = cartaoBanco.get();
		   aux = cartaoRepository.save(aux);
		 
		   resposta.setCodigoResposta(00);
		   resposta.setMensagemResposta("Sucesso.");
		   return ResponseEntity.ok(resposta);
		}
		
		 resposta.setCodigoResposta(05);
		 resposta.setMensagemResposta("Cartao invalido.");
		 return ResponseEntity.ok(resposta);
		
	} 

}
