package com.itau.ecommerce.model;

public class CartaoContrato {
	
	private String numeroCartao;
	private String nomeClienteCartao;
	private String codigoSeguranca;
	private int dataValidade;
	private double valor;
	
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public String getNomeClienteCartao() {
		return nomeClienteCartao;
	}
	public void setNomeClienteCartao(String nomeClienteCartao) {
		this.nomeClienteCartao = nomeClienteCartao;
	}
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}
	public int getDataValidade() {
		return dataValidade;
	}
	public void setDataValidade(int dataValidade) {
		this.dataValidade = dataValidade;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	@Override
	public String toString() {
		return "CartaoContrato [numeroCartao=" + numeroCartao + ", nomeClienteCartao=" + nomeClienteCartao
				+ ", codigoSeguranca=" + codigoSeguranca + ", dataValidade=" + dataValidade + ", valor=" + valor + "]";
	}
	

}
