package com.itau.ecommerce.model;

public class CartaoContratoResposta {
	
	private int codigoResposta;
	private String mensagemResposta;
	
	public int getCodigoResposta() {
		return codigoResposta;
	}
	public void setCodigoResposta(int codigoResposta) {
		this.codigoResposta = codigoResposta;
	}
	public String getMensagemResposta() {
		return mensagemResposta;
	}
	public void setMensagemResposta(String mensagemResposta) {
		this.mensagemResposta = mensagemResposta;
	}
	
}
