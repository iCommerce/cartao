package com.itau.ecommerce.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cartao {

	@Id
	private String numeroCartao;
	private String nomeClienteCartao;
	private String codigoSeguranca;
	private int dataValidade;
	private boolean ativo;
	private double saldoLimite;

	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public String getNomeClienteCartao() {
		return nomeClienteCartao;
	}
	public void setNomeClienteCartao(String nomeClienteCartao) {
		this.nomeClienteCartao = nomeClienteCartao;
	}
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}
	public int getDataValidade() {
		return dataValidade;
	}
	public void setDataValidade(int dataValidade) {
		this.dataValidade = dataValidade;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public double getSaldoLimite() {
		return saldoLimite;
	}
	public void setSaldoLimite(double saldoLimite) {
		this.saldoLimite = saldoLimite;
	}

	public boolean validaNomeClienteCartao (String nomeClienteCartao) {
		if (this.nomeClienteCartao.equals(nomeClienteCartao)) {
			return true;
		}else {
			return false;
		}
	}

	public boolean validaCodigoSeguranca (String codigoSeguranca) {
		if (this.codigoSeguranca.equals(codigoSeguranca)) {
			return true;
		}else {
			return false;
		}
	}

	public boolean validaDataValidade (int dataValidade) {
		if (this.dataValidade == dataValidade) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Cartao [numeroCartao=" + numeroCartao + ", nomeClienteCartao=" + nomeClienteCartao
				+ ", codigoSeguranca=" + codigoSeguranca + ", dataValidade=" + dataValidade + ", ativo=" + ativo
				+ ", saldoLimite=" + saldoLimite + "]";
	}

}
